package work.tavor.com.todosapp;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.ViewGroup;
import android.widget.TextView;


import android.support.v7.widget.RecyclerView;

import java.util.Collections;
import java.util.List;


public class CardAdpter extends RecyclerView.Adapter<CardAdpter.AdapterViewHolder>{

    List<Item> listItem;

    public CardAdpter(List<Item> lists)
    {
        this.listItem=lists;
    }

    @Override
    public CardAdpter.AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.list_card_view,parent,false);
        AdapterViewHolder holder= new AdapterViewHolder(v);
     return holder;
    }
    public void updateList(List<Item> data) {
        listItem = data;
        notifyDataSetChanged();
    }
        @Override
        public void onBindViewHolder(AdapterViewHolder holder, int position) {

            holder.bn.setBottom(listItem.get(position).getBtn());
            holder.taskText.setText(listItem.get(position).getTaskd());
        }


    public static class AdapterViewHolder extends RecyclerView.ViewHolder
    {
        TextView taskText;
        Button bn;
        public AdapterViewHolder(View item)
        {
            super(item);
            taskText= (TextView) item.findViewById(R.id.textView1);
            bn=(Button) item.findViewById(R.id.button2);
        }

    }
    public void addItem(int position, Item data) {
        listItem.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(String task) {
        for(int i=0;i<listItem.size();i++)
        {
            if(listItem.get(i).equals(task))
            {
                listItem.remove(i);
            }

        }


    }
    @Override
    public int getItemCount() {
        return listItem.size();
    }
}
