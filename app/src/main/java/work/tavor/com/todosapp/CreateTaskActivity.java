package work.tavor.com.todosapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

public class CreateTaskActivity extends AppCompatActivity {
  //  View v;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_task);
        Button backb=(Button) findViewById(R.id.button13);
        backb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText textView=(EditText)findViewById(R.id.description);
                String Task=textView.getText().toString();

                Intent passIntent = new Intent();
                passIntent.putExtra("text", Task);
                setResult(Activity.RESULT_OK, passIntent);
                finish();

            }
        });
    }

}
