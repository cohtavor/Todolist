package work.tavor.com.todosapp;
import android.widget.Button;
/**
 * Created by user on 10/11/2015.
 */
public class Item {
    private String taskd;
    private int btn;

    public void setTaskd(String taskd) {
        this.taskd = taskd;
    }

    public void setBtn(int btn) {
        this.btn = btn;
    }

    public int getBtn() {

        return btn;
    }

    public String getTaskd() {

        return taskd;
    }

    public Item(String taskd,int btn){

        this.taskd = taskd;
        this.btn = btn;
    }
}
