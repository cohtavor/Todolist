package work.tavor.com.todosapp;


import android.app.Activity;
import android.content.ClipData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class TaskListActivity extends AppCompatActivity  {


    RecyclerView mRecyclerView;
    LinearLayoutManager layoutManager;
     List<Item> listItem ;
    CardAdpter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
         final Button b=(Button) findViewById(R.id.buttonAdd);
        listItem= new ArrayList<Item>();
      listItem.add(new Item("task1",R.id.button2));
        listItem.add(new Item("task2",R.id.button2));
        listItem.add(new Item("task3",R.id.button2));


         adapter = new CardAdpter(listItem);
        mRecyclerView.setAdapter(adapter);


    }

    public void OnClickAdd(View v){
        Intent intent = new Intent(TaskListActivity.this,CreateTaskActivity.class);
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String task=new String(data.getStringExtra("text"));
                Item it=new Item(task,R.id.button2);
                adapter.addItem(0,it);


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult



}